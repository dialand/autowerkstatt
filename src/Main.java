public class Main {
    public static void main(String[] args) {
        //Szenario
        Werkstatt werkstatt1 = new Werkstatt();

        Auto auto1 = new Auto("Herr X");
        Auto auto2 = new Auto("Frau Mustername");
        Auto auto3 = new Auto("Herr Bond");
        Auto auto4 = new Auto("Frau X");
        auto1.kaputtgehen();
        auto2.kaputtgehen();
        auto3.kaputtgehen();
        auto4.kaputtgehen();

        //Werkstatt
        System.out.println("\nIn der Werkstatt:");
        werkstatt1.zurReparaturListeHinzufügen(auto1);
        werkstatt1.zurReparaturListeHinzufügen(auto2);
        werkstatt1.zurReparaturListeHinzufügen(auto3);
        werkstatt1.zurReparaturListeHinzufügen(auto4);

        //ein Auto wird fertig repariert
        werkstatt1.reparieren(auto1);
        //nach Fertigstellung der Reparatur: Informationen aktueller Status zu Anz der zu reparierenden Autos
        System.out.println("Es sind noch " + werkstatt1.getZurReparaturListe().size() + " Autos zur Reparatur da.");

        //Nachfrage: Status- auf welchem Platz ist mein Auto: z.B. Auto3
        System.out.println(werkstatt1.gebePlatzNrAuto(auto3));
    }


}
