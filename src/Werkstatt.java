import java.util.*;

public class Werkstatt {

    private LinkedList<Auto> zurReparaturListe;
    private Reparateur reparateur;


    public Werkstatt() {
        this.reparateur = new Reparateur(1);
        this.reparateur = new Reparateur(2);
        this.zurReparaturListe = new LinkedList<>();
    }

    Random rand = new Random();

    public void zurReparaturListeHinzufügen(Auto auto) {
        zurReparaturListe.add(auto);
        System.out.println(gebeReparaturplatzAn());
        vergebeAuftragsnummer(auto);
    }

    public void vergebeAuftragsnummer(Auto auto) {
        int auftragsnummer = rand.nextInt(100);//nächster Schritt: beim ersten Auftrag Auftragsnummer 1 vergeben, fortlaufend
        auto.setAuftragsnummer(auftragsnummer);
    }

    public String gebeReparaturplatzAn() {
        if (zurReparaturListe.size() > 2) {
            int platzNr = zurReparaturListe.size() - 2;
            return "Die Reparteuere sind voll ausgelastet.\nAuf der Warteliste ist das Auto auf Platz " + platzNr + ".\n";
        } else {
            return "Das Auto wird sofort repariert. Ein Reparateur ist frei.\n";
        }
    }


    public Queue<Auto> getZurReparaturListe() {
        return zurReparaturListe;
    }

    public void reparieren(Auto auto) {
        auto.setFunktionsfaehig(true);
        System.out.println("Das Auto wurde repariert und ist nun wieder funktionsfähig.");
        zurReparaturListe.poll();
        gebeNeueGesReihung();
    }

    public void gebeNeueGesReihung() {
        for (Auto auto : zurReparaturListe) {
            int autoPlatz = zurReparaturListe.indexOf(auto) + 1;
            System.out.println("Das Auto mit der Auftragsnummer " + auto.getAuftragsnummer() + " von " + auto.getBesitzer() +
                    " ist nun auf Platz " + autoPlatz);
            if (zurReparaturListe.indexOf(auto) < 2) {
                System.out.println("Es ist unter den beiden Autos, die gerade repariert werden.");
            } else {
                int platzNr = zurReparaturListe.size() - 2;
                System.out.println("Auf der Warteliste ist das Auto auf Platz " + platzNr + ".\n");
            }
        }
    }

    //jederzeit für beliebiges Auto: Reihung auswerfen lassen
    public int gebePlatzNrAuto(Auto auto) {
        int autoPlatz = zurReparaturListe.indexOf(auto) + 1;
        System.out.print("\nDie Autos auf Platz 1 und 2 werden gerade repariert. Das gewählte Auto ist auf Platz: ");
        return autoPlatz;
    }
}
