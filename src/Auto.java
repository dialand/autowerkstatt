public class Auto {
    private Boolean funktionsfaehig = true;
    private String besitzer;
    private int auftragsnummer;

    public Auto() {
        this.funktionsfaehig = funktionsfaehig;
    }

    public String getBesitzer() {
        return besitzer;
    }

    public int getAuftragsnummer() {

        return auftragsnummer;
    }

    public Auto(String besitzer) {
        this.funktionsfaehig = funktionsfaehig;
        this.besitzer = besitzer;
    }

    public void setAuftragsnummer(int auftragsnummer) {
        this.auftragsnummer = auftragsnummer;
    }

    public void setFunktionsfaehig(Boolean funktionsfaehig) {
        this.funktionsfaehig = funktionsfaehig;
    }

    public void kaputtgehen() {
        funktionsfaehig = false;
        System.out.println("Das Auto ist kaputtgegangen.");
    }
}
