Das Projekt der Werkstatt/ Autoreparatur ist als Übung entstanden, um mit der Umsetzung des
Szenarios den Transfer von realen Situationen in Java Code zu vertiefen und die Modellierung
zu festigen.
 
 Es handelt sich hierbei um eine Welt, in der 4 Autos als Beispiel
 kaputtgegangen sind. Jedes Auto hat einen Besitzer. Die Autos kommen nun
 zur Reparatur in eine Werkstatt. Dafür werden sie zur Reparaturliste hinzugefügt.
 Die Autos bekommen eine Auftragsnummer.
 In der Werkstatt arbeien 2 Reparateure.
  

Folgende User Stories sind umgesetzt:
-In der Werkstatt kann ein Auto repariert werden.
-Wenn ich ein Auto zur Reparatur bringe, dann wird mir automatisch mitgeteilt,
ob es sofort repariert werden kann oder ob es auf der Warteliste steht.

-Wenn in der Werkstatt ein Auto fertig repariert wurde,
dann wird automatisch der gesamten Status der Autos ausgegeben.

-Ich kann anzeigen lassen, wie viele Autos insgesamt aktuell 
zur Reparatur in der Werkstatt sind.

-Für ein Auto, das ich zur Werkstatt gebracht habe,
kann ich den aktuellen Status abfragen, sprich den
Status zwischen "in Reparatur" oder wenn es auf der Warteliste ist, den Wartelistenplatz
ausgeben lassen.


Für die Zukunft:
-Vergabe einer chronologischen Auftragsnummer, die sich nach dem historischen Auftragsverlauf erhöht
-eine Auftragasklasse mit Attributen: Reparateur, Auto und ID
-verschiedene Arten von Reparaturen und Reparaturdauer